#!/usr/bin/env wsapi.cgi
package.path = table.concat({
    'assets/?.lua',
    'libs/?.lua',
    'modules/?.lua',
    '',
}, ';') .. package.path

local orbit = require 'orbit'

local cfg = require 'cfg'
local items = require 'items'

module("wow_items", package.seeall, orbit.new)

-- helpers
local get_max_chars = function(t)
    local max = 0
    for _,v in pairs(t) do
        local i = 0
        for _,_ in pairs(v["where"]) do
            i = i + 1
        end
        if i > max then max = i end
    end

    return max
end

-- controllers
function index(web)
    return render_index(web, { index = true })
end

wow_items:dispatch_get(index, "/")

function search(web)
    local get = web.GET.q
    if not get or string.len(get) == 0 then web:redirect("/") end
    local data = {}

    local id = tonumber(get)

    if id then
        data = items:find_items_by_id(cfg, id)
    else
        data = items:find_items_by_name(cfg, get:lower())
    end

    return render_index(web, { db = data, search = get })
end

wow_items:dispatch_get(search, "/search")

function items_all(web)
    local data = items:find_all(cfg)
    return render_index(web, { db = data })
end

wow_items:dispatch_get(items_all, "/items")

function items_by_classes(web, id)
    if not id then return web:redirect("/") end

    local class, sub_class
    if string.find(id, "%.") then
        class, sub_class = string.match(id, "(%d+)%.(%d+)")
        sub_class = tonumber(sub_class)
    else
        class = id
    end

    class = tonumber(class)

    local data = items:find_items_by_class(cfg, class, sub_class)
    return render_index(web, { db = data })
end

wow_items:dispatch_get(items_by_classes, "/items=(.+)")

wow_items:dispatch_static("/css/.+", "/assets/data%.json", "/img/.+", "/js/.+")

-- views
function layout(web, args, inner)
    return html{
        head{
            title("WoW - Items DB"),
            meta{
                ["http-equiv"] = "Content-Type",
                content = "text/html; charset=utf-8"
            },
            script{
                type = 'text/javascript',
                "var wowhead_tooltips = { \"colorlinks\": true, \"iconizelinks\": true, \"renamelinks\": false }",
            },
            script{
                type = 'text/javascript',
                src = web:static_link('/js/jquery-2.0.3.min.js'),
            },
            script{
                type = 'text/javascript',
                src = web:static_link('/js/items.js'),
            },
            script{
                type = 'text/javascript',
                src = web:static_link('/js/jquery.tablesorter.min.js'),
            },
            link{
                rel = 'stylesheet',
                type = 'text/css',
                href = web:static_link('/css/style.css'),
                media = 'screen',
            },
        },
        body{
            div{
                id = "container",
                div{
                    id = "header",
                    div{
                        id = "search",
                        form{
                            method = "get",
                            action = web:static_link("/search"),
                            input{
                                id = "search-box",
                                type = "text",
                                name = "q",
                                value = (args.search or ""),
                            },
                        },
                    },
                    div{
                        id = "menu",
                        ul{
                            id = "nav",
                            _menu(web),
                        },
                        br(),
                    },
                },
                div{
                    id = "contents",
                    (H'table'{
                        id = "items",
                        class = "tablesorter",
                        thead{
                            th{ "Name" },
                            th{ "Total" },
                            th{
                                colspan = args.cols,
                                "Where",
                            },
                        },
                        inner,
                    }),
                },
            },
            script{
                type = 'text/javascript',
                src = web:link("http://static.wowhead.com/widgets/power.js"),
            },
        },
    }
end

function _menu(web)
    local res = {}
    -- TODO: move this to the cfg...
    local make_link = {
        [0] = true,
        [1] = true,
        [2] = true,
        [3] = true,
        [4] = true,
        [7] = true,
        [9] = true,
        [12] = true,
        [14] = true,
        [15] = true,
        [16] = true,
    }

    local c = {}
    for id_class, name_class in pairs(cfg["class"]) do
        if make_link[id_class] then
            local sub_class = {}
            for id_sub_class, name_sub_class in pairs(cfg["sub_class"][id_class]) do
                sub_class[#sub_class + 1] = li( a{ href = web:link("/items=" .. id_class .. "." .. id_sub_class), name_sub_class })
            end

            c[#c + 1] = li{ a{ href = web:link("/items=" .. id_class), name_class }, ul{ sub_class }}
        end
    end

    res[#res + 1] = li{ a{ href = web:link("/items"), "Database" }, ul{ c }}

    return res
end

function _item(web, args, cols)
    local name = args["name"]
    local quality = args["quality"]
    local class = args["itemClass"]
    local sub_class = args["itemSubClass"]
    local where = args["where"]
    local total, i = 0, 2

    local list = {}
    for char,count in pairs(where) do
        total = total + count
    end

    list[#list + 1] = td{ total }
    for char,count in pairs(where) do
        list[#list + 1] = td{ ("%s: %s"):format(char, count) }
        i = i + 1
    end

    if cols > i then
        local ins = cols - i
        for j=1,ins do
            list[#list + 1] = td{}
        end
    end

    return name, quality, class, sub_class, list
end

function render_index(web, args)
    if not args.db then
        if not args.index then
            return layout(web, args, p("No items found!"))
        else
            return layout(web, args)
        end
    else
        local res = {}
        local max = get_max_chars(args.db)
        local num_cols = max + 2

        if args.search then
            res[#res + 1] = h2("Search results for.. " .. i(args.search))
        end

        for id,t in pairs(args.db) do
            local name, quality, class, sub_class, where = _item(web, t, num_cols)

            res[#res + 1] = tr{
                td{
                    a{
                        href = ("%s%d"):format("http://www.wowhead.com/item=", id),
                        class = ("%s%d"):format("q", quality),
                        ("%s"):format(name),
                    },
                },
                where,
            }
        end

        args.cols = (max * 2)

        return layout(web, args, res)
    end
end

orbit.htmlify(wow_items, "layout", "_.+", "render_.+")

return _M

