#!/usr/bin/env bash
share="wow"
vol="/media/wow"
account="YOUR ACCOUNT NAME"

ret=$( mount | grep "on ${vol} type" > /dev/null )
if [ ! "$ret" ]; then
    sudo mount -t vboxsf "${share}" "${vol}"
fi

if [ ! -f "assets/data.lua" ]; then
    ln -s "${vol}/WTF/Account/${account}/SavedVariables/BagSync.lua assets/data.lua"
fi

