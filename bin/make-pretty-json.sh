#!/usr/bin/env bash
if [ -d "../assets" ]; then
    dir="../assets"
elif [ -d "assets" ]; then
    dir="assets/"
else
    exit 1
fi

dat="${dir}/data.json"
pretty="${dir}/data-pretty.json"

cat "${dat}" | python -mjson.tool > "${pretty}"

