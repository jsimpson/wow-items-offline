#!/usr/bin/env lua
require 'luacurl'
require 'json'

local blizzard = {}
local next = next
local verbose = true

URL = {
    ["base"] = "api/wow/",
    ["classes"] = "classes",
    ["data"] = "data/",
    ["host"] = "http://us.battle.net/",
    ["item"] = "item/",
}

local write = function(t)
    return function(stream, buf)
        table.insert(t, buf)
        return string.len(buf)
    end
end

local getURL = function(cat, id, ...)
    local base = string.format("%s%s", URL["host"], URL["base"])
    local str

    if cat == "item" then
        str = string.format("%s%s%d", base, URL["item"], id)
    elseif cat == "data" then
        base = string.format("%s%s", base, URL["data"])
        if arg[1] == "item-classes" then
            str = string.format("%s%s%s", base, URL["item"], URL["classes"])
        end
    end

    return str
end

local fetchData = function(url)
    local t = {}
    local c, data, ret

    c = curl.new()
    c:setopt(curl.OPT_FAILONERROR, true)
    c:setopt(curl.OPT_VERBOSE, verbose or false)
    c:setopt(curl.OPT_WRITEFUNCTION, write(t))
    c:setopt(curl.OPT_URL, url)
    c:setopt(curl.OPT_USERAGENT, "luacurl-agent/1.0")

    ret = c:perform()
    if not ret then return end

    c:close()

    data = table.concat(t, "")

    return json.decode(data)
end

function blizzard:getItem(id)
    if not id or not type(id) == "number" then return end

    local url = getURL("item", id)
    if not url then return end

    local d = fetchData(url)

    return d["name"], d["quality"], d["itemClass"], d["itemSubClass"]
end

return blizzard

