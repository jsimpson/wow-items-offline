#!/usr/bin/env lua
require 'data'
require 'persistence'
require "logging.file"

local logger = logging.file("log%s.log", "%m/%d/%Y")

local blizzard = require 'blizzard'
local json = require 'json'

local db_items = BagSyncDB
local db_guild = BagSyncGUILD_DB

local cache = persistence.load("assets/cache.lua") or {}

local items = {}

local getBlizzardItemData = function(id)
    local name, quality, class, subclass

    if not cache[id] then
        cache[id] = {}
        name, quality, class, subclass = blizzard:getItem(id)

        cache[id]["name"] = name
        cache[id]["quality"] = quality
        cache[id]["itemClass"] = class
        cache[id]["itemSubClass"] = subclass

        persistence.store("assets/cache.lua", cache)
    else
        name = cache[id]["name"]
        quality = cache[id]["quality"]
        class = cache[id]["itemClass"]
        subclass = cache[id]["itemSubClass"]
    end

    return name, quality, class, subclass
end

local getItemsDB = function(t)
    if not t then return end

    local d = {}
    for _,tbl in pairs(t) do
        for k,v in pairs(tbl) do
            local id, count = v, 1
            if string.find(v, '[,"]') then
                id, count = string.match(v, "(%w+),(%w+)")
            end

            id, count = tonumber(id), tonumber(count)

            if not d[id] then
                d[id] = count
            else
                local n = d[id]
                d[id] = count + n
            end
        end
    end

    return d
end

local get_servers = function(t)
    if not t then return end

    local s = {}

    for i=1,#t do
        s[#s + 1] = t[i]
    end

    return s
end

local get_characters = function(t, t_char)
    if not t or not t_char then return end

    local c = {}

    for i=1,#t do
        for _,name in pairs(t_char[i]) do
            c[#c + 1] = name
        end
    end

    return c
end

local get_items_characters = function(servers, characters, cfg)
    if not servers or not characters or not cfg then return end
    local categories = cfg["categories"]
    local blacklist = cfg["blacklist"]

    local data = {}

    for _,server in pairs(servers) do
        local t = db_items[server]
        if not t then return end

        for _,character in pairs(characters) do
            if not t[character] then return end

            for _,cat in pairs(categories) do
                if t[character][cat] then
                    local d = getItemsDB(t[character][cat])
                    if not d then return end

                    for id,count in pairs(d) do
                        local n = 0

                        if not blacklist[id] then
                            if not data[id] then
                                local name, quality, class, subclass = getBlizzardItemData(id)

                                data[id] = {}
                                data[id]["name" ] = name
                                data[id]["quality"] = quality
                                data[id]["itemClass"] = class
                                data[id]["itemSubClass"] = subclass

                                data[id]["where"] = {}
                            end

                            if not data[id]["where"][character] then
                                data[id]["where"][character] = nil
                            else
                                n = data[id]["where"][character]
                            end

                            data[id]["where"][character] = count + (n or 0)
                        end
                    end
                end
            end
        end
    end

    return data
end

local get_items_guilds = function(servers, data, cfg)
    if not servers or not data or not cfg then return end

    local blacklist = cfg["blacklist"]

    for _,server in pairs(servers) do
        local t = db_guild[server]
        if not t then return end

        for guild_name,bank in pairs(t) do
            if not bank then return end

            for _,v in pairs(bank) do
                local n = 0
                local id, count = v, 1

                if string.find(v, '[,"]') then
                    id, count = string.match(v, "(%w+),(%w+)")
                end

                id, count = tonumber(id), tonumber(count)

                if not blacklist[id] then
                    if not data[id] then
                        local name, quality, class, subclass = getBlizzardItemData(id)

                        data[id] = {}
                        data[id]["name" ] = name
                        data[id]["quality"] = quality
                        data[id]["itemClass"] = class
                        data[id]["itemSubClass"] = subclass

                        data[id]["where"] = {}
                    end

                    if not data[id]["where"][guild_name] then
                        data[id]["where"][guild_name] = nil
                    else
                        n = data[id]["where"][guild_name]
                    end

                    data[id]["where"][guild_name] = count + (n or 0)
                end
            end
        end
    end

    return data
end

function items:find_items_by_class(cfg, class, sub_class)
    local data = items:find_all(cfg)
    local ret = {}

    for id,t in pairs(data) do
        if t["itemClass"] == class then
            if sub_class then
                if t["itemSubClass"] == sub_class then
                    ret[id] = t
                end
            else
                ret[id] = t
            end
        end
    end

    return ret
end

function items:find_items_by_name(cfg, item)
    local data = items:find_all(cfg)
    local ret = {}

    for id,t in pairs(data) do
        if item and t["name"] and string.match(t["name"]:lower(), item) then
            ret[id] = t
        end
        if not t["name"] then logger:error("nil id: " .. id) end
    end

    return ret
end

function items:find_items_by_id(cfg, id)
    local data = items:find_all(cfg)
    local ret = {}

    if data[id] then
        ret[id] = data[id]
    end

    return ret
end

function items:find_all(cfg)
    local c, s, data = {}, {}, {}

    s = get_servers(cfg["server"])
    c = get_characters(s, cfg["characters"])

    data = get_items_characters(s, c, cfg)
    data = get_items_guilds(s, data, cfg)

    return data
end

function items:saveItemsToJSON(t, cfg)
    if not t then return end

    local f = io.open(cfg["files"]["json"], "w")
    if f then
        local out = json.encode(t)
        f:write(out)
        f:close()
    end
end

return items

