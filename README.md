## WHAT IS THIS?
This is a really small Orbit app that works with the WoW addon [BagSync](http://www.wowinterface.com/downloads/info15351-BagSync.html). It creates a local database of items that belong to your characters and starts up a web app to let you view the inventory offline. It connects to [Blizzard's API](http://blizzard.github.io/api-wow-docs/) to grab information about the item(s) and also employs some very generic caching mechanisms so that we only hit the Blizzard API a single time per item. Additional features:
* Simple search
* Simple sorting
* Filters based on item category and subcategory
* Aggregation of item counts and which characters have how many
* WoWhead links and tooltips

This is completely unsupported and was written primarily to check out Orbit but also for my personal consumption.

## HOW TO USE IT
If you're really that interested you'll first have to resolve the dependency chains. Then edit the assets/cfg.lua file to define the server(s) and character(s) you want the app to parse inventory for. You'll also have to obtain the BagSync.lua file from your WoW installation's WTF/Account directory. When I originally created this I was running it in a VM via VirtualBox. There is a small bash script that will naively symlink the BagSYnc.lua file for you. This also requires that you first mount the host, so you'll have to configure VirtualBox for this as well. I used the share "wow" with a volume at "/media/wow". You'll also need to edit it the shell script for your specific account name. After that, simply execute the command:
    orbit index.lua
